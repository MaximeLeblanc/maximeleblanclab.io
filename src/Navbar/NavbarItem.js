import React, { Component } from "react";

export default class NavbarItem extends Component {
  /*
    <a
    href="#"
    class="block sm:flex bg-gray-900 text-white px-3 py-2 rounded-md text-base sm:text-sm font-medium"
    aria-current="page"
    >
      {this.props.name}
    </a>
  */
  render() {
    return (
      <a
        href="#"
        class="block sm:flex text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-base sm:text-sm font-medium"
      >
        {this.props.name}
      </a>
    );
  }
}
